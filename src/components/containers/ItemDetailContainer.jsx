import {useState, useEffect} from 'react'
import { useParams } from 'react-router';
import { getFirestore } from '../../services/getFirebase';

import ItemDetail from '../ItemDetail/ItemDetail';



const ItemDetailContainer = () => {
    const [item, setItem] = useState({})
    const {id} = useParams()

    useEffect(() => {
        
        const dbQuey = getFirestore()
        dbQuey.collection('Items').doc(id).get()
        .then(resp =>  setItem( { id: resp.id , ...resp.data() } ))
        .catch(err=>console.log(err))
        //.finally(()=> setLoading(false))
    
    }, [id])
   
    console.log(id);
    console.log(item);
    return (
        <div>
            <ItemDetail item={item} />
        </div>
    )
}

export default ItemDetailContainer
