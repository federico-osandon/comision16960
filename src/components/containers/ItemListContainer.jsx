import {useState, useEffect} from 'react'
import { useParams } from 'react-router-dom'
// import { Input } from '../../../clases/clase9/Input'
import { getFetch } from '../../utils/Mock'
import ItemList from '../../components/ItemList/ItemList'
import { getFirestore } from '../../services/getFirebase'



function ItemListContainer() {
    const [loading, setLoading] = useState(true)
    const [items, setItems] = useState([])
    const [item, setItem] = useState({})

    const { idCategoria } = useParams()
   
    
    
    //console.log(db)
    
    useEffect(() => {
        
        
        if (idCategoria) {
            const db = getFirestore()
            db.collection('Items').where('categoria', '==', idCategoria).get() //toda la colección
            .then(resp => setItems( resp.docs.map(it => ({id: it.id, ...it.data() }) )) )            
        } else {
            const db = getFirestore()
            db.collection('Items').get() //toda la colección
            .then(resp => setItems( resp.docs.map(it => ({id: it.id, ...it.data() }) )) )
            .catch(err => console.log(err) )
            .finally(()=> setLoading(false))            
        }


        // if(category===undefined){
        //     getFetch
        //     .then((resp)=> setItems(resp) )     //guardar en el estado
        //  }else{
        //     getFetch
        //      .then((resp)=> setItems(resp.filter( r => category===r.categoria)) ) 
        // }
    }, [idCategoria])

    //ejemplo de evento
    const handleClick=(e)=>{
        e.preventDefault() 
        //setBool(!bool)
    }
    
    const handleAgregar=()=>{
       setItems([
           ...items,
           { id: "8", name: "Gorra 7", url: 'https://www.remerasya.com/pub/media/catalog/product/cache/e4d64343b1bc593f1c5348fe05efa4a6/r/e/remera_negra_lisa.jpg', categoria: "remera" , price: 2 }
       ])
    }

    console.log(idCategoria);
    console.log(items);
    //console.log('itemListContainer');
   

    return (
        <>
            <button onClick={handleClick}>Cambiar estado </button>           
            <button onClick={handleAgregar}>Agregar Item </button>   
            {loading ? <h2>CArgando ...</h2> : 
                <ItemList personas={items} />            
            }        
            {/* <button onKeyDown={handleClick} >click</button> */}
        </>
    )
}

export default ItemListContainer
