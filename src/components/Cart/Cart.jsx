import {useState} from 'react'
import { useCartContext } from '../../context/cartContext'
import firebase from 'firebase/app'
import 'firebase/firestore'
import { getFirestore } from '../../services/getFirebase'

const Cart = () => {  
   const [formData, setFormData] = useState({
       name:'',
       phone:'',
       email: ''
   })
   const {carList, precioTotal, borrarLista} = useCartContext()

   const generarOrden = (e) =>{
       e.preventDefault()
       
    let orden = {}
    orden.date = firebase.firestore.Timestamp.fromDate(new Date());    
    orden.buyer = formData
    orden.total = precioTotal();
    orden.items = carList.map(cartItem => {
        const id = cartItem.item.id;
        const nombre = cartItem.item.nombre;
        const precio = cartItem.item.precio * cartItem.quantity;
        
        return {id, nombre, precio}   
    })
    
   

    // const updateQuery = dbQuery.collection('Items')
    // updateQuery.doc('Hdvmy1E9DaXFfM2hNT6B').update({
    //     stock: 9
    // })
    // .then(result => console.log('esta actulalizado'))

    const db = getFirestore();
    const ordersCol = db.collection('orders');
    ordersCol.add(orden)
    .then((IdDocumento)=>{
        console.log(IdDocumento.id)
    })
    .catch( err => {
        console.log(err);
    })
    .finally(()=>{
        borrarLista()
        setFormData({
            name:'',
            phone:'',
            email: ''
        })
        console.log('terminó la compra')
    })



    //Actualiza todos los items que estan en el listado de Cart del CartContext

    const itemsToUpdate = db.collection('Items').where(
        firebase.firestore.FieldPath.documentId(), 'in', carList.map(i=> i.item.id)
    )

    const batch = db.batch();
    
    // por cada item restar del stock la cantidad de el carrito

    itemsToUpdate.get()
    .then( collection=>{
        collection.docs.forEach(docSnapshot => {
            batch.update(docSnapshot.ref, {
                stock: docSnapshot.data().stock - carList.find(item => item.item.id === docSnapshot.id).quantity
            })
        })

        batch.commit().then(res =>{
            console.log('resultado batch:', res)
        })
    })

    
    console.log(orden)
}

const handleChange=(e)=>{
    setFormData(
        {
            ...formData,
            [e.target.name]: e.target.value
        }

    )
}
console.log(formData)
    return (
        <>
            <h1>Hola soy Cart</h1>
            {carList.map(item => <h2 key={item.item.id} >{item.item.nombre}</h2>)}
            <button onClick={()=>generarOrden()}>Realizar Compra</button>

            <form onSubmit={generarOrden} onChange={handleChange} >
                <input type='text' name='name' placeholder='name' value={formData.name}/>
                <input type='text' name='phone'placeholder='tel' value={formData.phone}/>
                <input type='email' name='email'placeholder='email' value={formData.email}/>
                <button>Enviar</button>
            </form>
        </>
    )
}

export default Cart
