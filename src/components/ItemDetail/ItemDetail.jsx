import { useContext } from 'react'
import { AppContext } from '../../App'


import { useCartContext } from '../../context/cartContext'

import ItemCount from '../ItemCount'


const ItemDetail = ( {item} ) => {    
    const {state}  = useContext(AppContext)
    const {addToCart, carList} = useCartContext()
    
    const onAdd=(cant)=>{       
        console.log(cant)
        addToCart( item, cant)
    }   
    
    console.log(carList);
    return (
        <>            
            <h2>{item.nombre}</h2>  
            <h2>{item.precio}</h2> 
            <img className='w-25' src={item.urlImage} alt='' /> 

            <ItemCount initial={1} stock={5} onAdd={onAdd} /> 
            {state.map(i => <label key={i.id} > {i.nombre} |</label>)}

        </>
    )
}

export default ItemDetail
