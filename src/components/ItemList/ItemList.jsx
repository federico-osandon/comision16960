import { memo } from 'react'
import Item from './Item'


// memo(fn, fn) 

const ItemList = memo(
    ({personas}) => {
        
            console.log('soy item list')
            return (
                <>
                   {personas.map(persona => <Item persona={persona} />  )}
                </>
            )
        }
, (oldProps, newProps)=> oldProps.personas.length === newProps.personas.length)


    
    
    export default ItemList

