import firebase from "firebase"

import 'firebase/firestore'


const firebaseConfig = {
    apiKey: "AIzaSyBoG67i8K7ppBisGsIinowtDswd1_QOVxQ",
    authDomain: "comision16960.firebaseapp.com",
    projectId: "comision16960",
    storageBucket: "comision16960.appspot.com",
    messagingSenderId: "147734984990",
    appId: "1:147734984990:web:4d588a606ab6ffc318310f"
};


const app = firebase.initializeApp(firebaseConfig)


// export function getFirebase(){
//     return app
// }

export function getFirestore(){    
    return firebase.firestore(app)
}


