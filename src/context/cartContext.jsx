import {useState, createContext, useContext } from 'react'

const cartContext= createContext([])

export const useCartContext = () => useContext(cartContext) 


export default function CartContextProvider ({children}) {
    const [carList, setCarList] = useState([])

    const addToCart = (item, quantity) => {

        const index = carList.findIndex(i => i.item.id === item.id)//-1, pos
        console.log(index)
    
          if (index > -1) {
            const oldQy = carList[index].quantity
    
            carList.splice(index, 1)
            setCarList([...carList, { item, quantity: quantity + oldQy}])
          }
          else {
            setCarList([...carList, {item, quantity}])
          }
      }

      const deleteFromCart = (item) => {
            //Verificamos si esta en el carrito  

            const deleteProduct = carList.filter((prod) => prod.item.id !== item.item.id);
        
            setCarList([...deleteProduct]);
        };  


        const iconCart = () => {
            return carList.reduce( (acum, valor)=> acum + valor.quantity, 0) 
            //return product.length
        }


        const precioTotal =()=>{
            return carList.reduce((acum, valor)=>(acum + (valor.quantity * valor.item.precio)), 0) 
        }

        function borrarLista(){
            setCarList([])
        }





    console.log(carList);
    return(
        <cartContext.Provider value={{
            carList,
            addToCart,
            deleteFromCart,
            iconCart,
            precioTotal,
            borrarLista
        }}>
            {children}
        </cartContext.Provider>
    )
}



