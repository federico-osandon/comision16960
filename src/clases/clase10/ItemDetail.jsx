import { useState, useContext } from 'react'

import { ContextApp } from '../../App'
import { useCartContext } from '../../context/cartContext'



import ItemCount from '../ItemCount'


const ItemDetail = ({item}) => {
    //const [cambiarBoton, setCambiarBoton] = useState(true)
    //aca     
    
    const {addToCart} = useCartContext()
    
    const { state }  = useContext(ContextApp)
    
    const onAdd=(cant)=>{
        console.log(cant) 
        addToCart({item: item, cantidad: cant })
    }  
    console.log(addToCart)

    return (
        <>
            {state.map(item => <p> {item.nombre} |</p>) }
           <h2>{item.id}</h2>  
           <h2>{item.name}</h2>  
           <h2>{item.age}</h2> 
           <img src={item.foto} alt='' /> 
           <ItemCount initial={1} stock={5} onAdd={onAdd} />    

           {/* {
               cambiarBoton ?
               <ItemCount initial={1} stock={5} onAdd={onAdd} />
               :
               <div>
                <Link to="/cart">
                    <button>Terminar Compra</button>
                </Link>
                <Link to="/" >
                        <button >Seguir Comprando</button>      
                    </Link>
               </div>
           }  */}

        </>
    )
}

export default ItemDetail
