import { useState, createContext  } from 'react'

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import { NabVar } from './components/NavBar/NabVar';
import Cart from './components/Cart/Cart';
import ItemListContainer from './components/containers/ItemListContainer';
import ItemDetailContainer from './components/containers/ItemDetailContainer';
import CartContextProvider from './context/cartContext'

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'

const categorias = [
    {nombre: 'Remeras', id:'remeras', descripcion: 'esto son remeras'},
    {nombre: 'Gorrar', id:'gorras', descripcion: 'esto son gorras'},
    
]


export const ContextApp = createContext('fede')//contexto creado 


function App() {


    const [state, setState] = useState(categorias)
    //console.log(ContextApp)

    function setearStatte(item) {
        setState(item)
    }



    //console.log(cartContext)
    return (
        <CartContextProvider>
            <ContextApp.Provider value={ {state, setearStatte} } >
                <Router>
                    <div 
                        className="App border border-3 border-primary"
                        //onClick={handleClick}
                    >
                        <NabVar />
                        <Switch>
                            <Route exact path='/'  >
                                <ItemListContainer greeting={'hola soy item list container'} />  
                            </Route>

                            <Route path='/categoria/:idCategoria' component={ItemListContainer} />

                            <Route exact path='/detalle/:id' component={ItemDetailContainer} />

                            <Route exact path='/cart' component={Cart} />                    
                            
                        </Switch>
                    </div>
                </Router>
            </ContextApp.Provider>
        </CartContextProvider>
    );
}

export default App;