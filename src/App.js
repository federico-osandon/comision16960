import { useState, createContext } from 'react'
import { BrowserRouter, Switch, Route } from "react-router-dom";
import { NabVar } from './components/NavBar/NabVar';
import Cart from './components/Cart/Cart';
import ItemListContainer from './components/containers/ItemListContainer';
import ItemDetailContainer from './components/containers/ItemDetailContainer';
import CartContextProvider from './context/cartContext';


import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'

export const categorias = [
    {nombre: 'Remeras', id:'remeras', descripcion: 'esto son remeras'},
    {nombre: 'Gorrar', id:'gorras', descripcion: 'esto son gorras'},
    
]


export const AppContext = createContext()

console.log(AppContext)

function App() {
    const [state, setState] = useState(categorias)
    //console.log(ContextApp)

    function setearStatte(item) {
        setState(item)
    }

    //console.log(cartContext)
    return (
        <CartContextProvider >
            <AppContext.Provider value={{state, setState, setearStatte}} >
                <BrowserRouter>
                    <div 
                        className="App"
                        //onClick={handleClick}
                    >
                        <NabVar />
                        <Switch>
                            <Route exact path='/'  >
                                <ItemListContainer greeting={'hola soy item list container'} />  
                            </Route>

                            <Route path='/categoria/:idCategoria' component={ItemListContainer} />

                            <Route exact path='/detalle/:id' component={ItemDetailContainer} />

                            <Route exact path='/cart' component={Cart} />                    
                            
                        </Switch>
                    </div>
                </BrowserRouter>
            </AppContext.Provider>
        </CartContextProvider>
           
    );
}

export default App;